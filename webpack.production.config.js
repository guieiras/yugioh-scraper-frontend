/* eslint-disable */

var webpack = require('webpack')
var config = require('./webpack.config.js')

config.devtool = 'cheap-module-source-map'
config.plugins.push(
  new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('production')
    }
  })
)

module.exports = config