import React from 'react'
import { Router, Route, IndexRoute, hashHistory } from 'react-router'

import App from './template/App.jsx'
import { HomePage, NotFoundPage } from './pages'
import DecksPage from './pages/decks'
import PricesPage from './pages/prices'

export default class AppRouter extends React.Component {
  render() {
    return <Router history={hashHistory}>
      <Route path='/' component={App}>
        <IndexRoute component={HomePage}/>
        <Route path='/decks'>
          <IndexRoute component={DecksPage.List}/>
        </Route>
        <Route path='/decks/new' component={DecksPage.New}/>
        <Route path='/decks/:id' component={DecksPage.Show}/>
        <Route path='/prices/search' component={PricesPage.Search}/>
        <Route path='*' component={NotFoundPage}/>
      </Route>
    </Router>
  }
}
