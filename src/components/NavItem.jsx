import React from 'react'
import { Link } from 'react-router'

export default class NavItem extends React.Component {
  static get propTypes() { return { to: React.PropTypes.string } }

  render() {
    return <li role="presentation">
      <Link to={this.props.to}>{this.props.children}</Link>
    </li>
  }
}
