import React from 'react'

import NotFoundPage from '../pages/NotFoundPage.jsx'
import LoadingPage from '../pages/LoadingPage.jsx'

export default class Loader extends React.Component {
  static get propTypes() {
    return { 
      loading: React.PropTypes.bool,
      show: React.PropTypes.bool
    }
  }

  render() {
    if(this.props.loading) {
      return <LoadingPage/>
    } else if(this.props.show) {
      return <div>
        { this.props.children }
      </div>
    } else {
      return <NotFoundPage/>
    }
  }
}
