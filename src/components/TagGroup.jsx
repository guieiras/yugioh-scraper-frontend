import React from 'react'
import { Badge } from 'react-bootstrap'

export default class TagGroup extends React.Component {
  static get propTypes() {
    return { tags: React.PropTypes.array }
  }

  render() {
    return <div>
      { this.props.tags.map((tag, idx) => {
        return <Badge key={idx} style={{marginRight: '5px'}}>{tag}</Badge> 
      }) }
    </div>
  }
}
