import React from 'react'
import { Col, Row } from 'react-bootstrap'
import { Text } from 'react-localize'
import DeckCardsComponent from './CardsComponent.jsx'

export default class BoxComponent extends React.Component {
  static get propTypes() {
    return { cards: React.PropTypes.object }
  }

  render() {
    let {main, extra, side} = this.props.cards
    return <div>
      <Row>
        <Col sm={12} md={6}>
          <h3><Text message='decks.attributes.main'/></h3>
          <DeckCardsComponent cards={main}/>
        </Col>
        <Col sm={12} md={6}>
          <h3><Text message='decks.attributes.side'/></h3>
          <DeckCardsComponent cards={side}/>
          <h3><Text message='decks.attributes.extra'/></h3>
          <DeckCardsComponent cards={extra}/>
        </Col>
      </Row>
    </div>
  }
}
