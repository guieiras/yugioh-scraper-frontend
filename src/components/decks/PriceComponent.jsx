import React from 'react'
import { Text } from 'react-localize'
import Localize from '../Localize.jsx'
export default class PriceComponent extends React.Component {
  static get propTypes() {
    return { cards: React.PropTypes.array }
  }

  render() {
    let paragraph = { margin: 0, padding: 0 }
    let prices = this.comparer()
    return <div>
      <p style={paragraph}>
        <strong><Text message='cards.price.minimum'/>: </strong>
        <Localize target={prices.minimum} type='currency'/>
      </p>
      <p style={paragraph}>
        <strong><Text message='cards.price.medium'/>: </strong>
        <Localize target={prices.medium} type='currency'/>
      </p>
      <p style={paragraph}>
        <strong><Text message='cards.price.maximum'/>: </strong>
        <Localize target={prices.maximum} type='currency'/>
      </p>
    </div>
  }

  availableCardPrices(stores) {
    return stores.reduce((memo, store) => {
      if(store && store.price && store.available !== 0) {
        memo.push(store.price)
      }
      return memo
    }, [])
  }

  comparer() {
    let { cards } = this.props
    return cards.reduce((memo, card) => {
      let prices = this.availableCardPrices(card.stores)
      let minimum, medium, maximum
      if(prices.length > 0) {
        minimum = prices.reduce((result, price) => {
          return !result || price < result ? price : result
        }, 0)
        maximum = prices.reduce((result, price) => {
          return price > result ? price : result
        }, 0)
        medium = prices.reduce((result, price) => {
          return price + result
        }, 0) / prices.length
      } else {
        [minimum, maximum, medium] = [0, 0, 0]
      }
      memo.minimum += minimum
      memo.medium += medium
      memo.maximum += maximum
      return memo
    }, {
      minimum: 0,
      medium: 0,
      maximum: 0
    })
  }
}
