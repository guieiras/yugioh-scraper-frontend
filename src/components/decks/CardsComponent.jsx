import React from 'react'
import _ from 'lodash'
import { Col, Row } from 'react-bootstrap'

import CardRow from '../cards/RowComponent.jsx'

export default class CardsComponent extends React.Component {
  static get propTypes() {
    return { cards: React.PropTypes.array }
  }

  render() {
    return <div>
      <Row>
        { this._aggregateCards().map((card) => {
          return <Col md={12} key={card.id}>
            <CardRow card={card}/>
          </Col>
        }) }
      </Row>
    </div>
  }

  _aggregateCards() {
    let { cards } = this.props
    let aggregation = _.groupBy(cards, 'id')
    return Object.keys(aggregation).map((id) => {
      let card = aggregation[id][0]
      card.count = aggregation[id].length
      return card
    })
  }
}
