import React from 'react'
import { Col, Row } from 'react-bootstrap'

import CardRow from '../cards/RowComponent.jsx'

export default class PricesComponent extends React.Component {
  static get propTypes() {
    return { prices: React.PropTypes.array }
  }

  render() {
    let { prices } = this.props
    return <Row>
      { prices.map((card, idx) => {
        return <Col md={12} key={idx}>
          <CardRow card={card}/>
        </Col> 
      }) }
    </Row>
  }
}
