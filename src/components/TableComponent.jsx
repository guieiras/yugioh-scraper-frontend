import React from 'react'
import { Table } from 'react-bootstrap'

export default class HomePage extends React.Component {
  static get propTypes() {
    return { headers: React.PropTypes.array }
  }

  render() {
    return <Table striped bordered hover>
      <thead>
        <tr>
          { this.props.headers.map((item, idx) => <th key={idx}>{item}</th>) }
        </tr>
      </thead>
      <tbody>
        { this.props.children }
      </tbody>
    </Table>
  }
}
