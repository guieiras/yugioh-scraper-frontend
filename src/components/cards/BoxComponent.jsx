import React from 'react'
import { Thumbnail } from 'react-bootstrap'

export default class BoxComponent extends React.Component {
  static get propTypes() {
    return { card: React.PropTypes.object }
  }

  render() {
    let {name, stores} = this.props.card
    return <Thumbnail src={this._imageUrlFor(name)}>
      <p><strong>{name}</strong></p>
      <p>{this._priceText(stores)}</p>
    </Thumbnail>
  }

  _imageUrlFor(cardName) {
    return `http://yugiohprices.com/api/card_image/${cardName}`
  }

  _priceText(stores) {
    let store = stores.reduce((memo, store) => {
      if(memo && store) {
        return store.available > 0 && store.price && store.price < memo.price ? store : memo 
      } else if(store && store.available > 0 && store.price) {
        return store
      } else {
        return null 
      }
    }, null)
    
    return store ? `Best price: R$ ${store.price} on ${store.storeId}` : 'Unavaiable'
  }

}
