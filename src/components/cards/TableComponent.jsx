import React from 'react'
import _ from 'lodash'
import { Table } from 'react-bootstrap'
import { Text } from 'react-localize'

import Localize from '../Localize.jsx'

export default class TableComponent extends React.Component {
  static get propTypes() {
    return { stores: React.PropTypes.array, hidden: React.PropTypes.bool }
  }

  render() {
    let { stores, hidden } = this.props
    stores = _.orderBy(stores, 'price', 'asc')
    return <Table bordered className={hidden ? 'hidden' : ''}>
      <thead>
        <tr>
          <th><Text message='cards.attributes.store'/></th>
          <th><Text message='cards.attributes.price'/></th>
          <th><Text message='cards.attributes.available'/></th>
        </tr>
      </thead>
      <tbody>
        { stores.map((store, idx) => {
          return <tr key={idx}>
            <td>{store.storeId}</td>
            <td><Localize target={store.price} type='currency'/></td>
            <td>{store.available >= 0 ? store.available : <Text message='cards.available.available'/>}</td>
          </tr>
        }) }
      </tbody>
    </Table>
  }
}
