import React from 'react'
import { Badge, Col, Row, Well } from 'react-bootstrap'
import FontAwesome from 'react-fontawesome'
import { Text } from 'react-localize'

import CardTable from './TableComponent.jsx'
import Localize from '../Localize.jsx'

export default class RowComponent extends React.Component {
  static get propTypes() {
    return { card: React.PropTypes.object }
  }

  constructor(props) {
    super(props)
    this.state = { collapsed: true }

    this._handleCollapse = this._handleCollapse.bind(this)
  }

  render() {
    let { name, stores, count } = this.props.card
    count = count ? `${count}x ` : ''
    return <Well style={{padding: '7px', marginBottom: '5px'}}>
      <a className='pull-right' onClick={this._handleCollapse} style={{cursor: 'pointer', color: 'grey'}}>
        <FontAwesome name={this.state.collapsed ? 'chevron-down' : 'chevron-up'}/>
      </a>
      <Badge className='pull-right' style={{marginTop: '3px', marginRight: '5px'}}>{this._bestPrice(stores)}</Badge>
      <p style={{fontSize: '15px', margin: '0'}}>{count}<strong>{name}</strong></p>
      <Row style={{marginTop: '8px'}} className={(this.state.collapsed ? 'hidden' : '')}>      
        <Col lg={2} className={'visible-lg'}>
          <img style={{width: '80px', verticalAlign: 'top'}} src={this._imageUrlFor(name)}/>
        </Col>
        <Col lg={10} md={12}>
          <CardTable stores={stores} hidden={this.state.collapsed}/>
        </Col>
      </Row>
    </Well>
  }

  _handleCollapse() {
    this.setState({collapsed: !this.state.collapsed})
  }

  _imageUrlFor(cardName) {
    return `https://yugiohprices.com/api/card_image/${cardName}`
  }

  _bestPrice(stores) {
    let store = stores.reduce((memo, store) => {
      if(memo && store) {
        return store.available !== 0 && store.price && store.price < memo.price ? store : memo 
      } else if(store && store.available !== 0 && store.price) {
        return store
      } else {
        return null 
      }
    }, null)
    return store ? <Localize target={store.price} type='currency'/> : <Text message='cards.box.unavailable'/>
  }
}
