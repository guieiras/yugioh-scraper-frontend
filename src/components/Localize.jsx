import React from 'react'

export default class Localize extends React.Component {
  static get propTypes() { return { target: React.PropTypes.any, type: React.PropTypes.string }}

  render() {
    const { type, target } = this.props
    return <span>{this.localize(target, type)}</span>
  }

  localize(target, type) {
    switch(type) {
    case 'currency':
      return this._localizeCurrency(target)
    default:
      return target
    }
  }

  _localizeCurrency(target) {
    if(typeof target == 'number') {
      return `R$ ${target.toFixed(2).replace(/\./, ',')}`
    } else {
      return target
    }
  }
}
