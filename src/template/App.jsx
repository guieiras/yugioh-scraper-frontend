import React from 'react'
import { Grid } from 'react-bootstrap'

import Header from './Header.jsx'

export default class App extends React.Component {
  render() {
    return <div>
      <Header />
      <Grid>
        { this.props.children }
      </Grid>
    </div>
  }
}
