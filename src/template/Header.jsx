import React from 'react'
import { Nav, Navbar } from 'react-bootstrap'
import { Text } from 'react-localize'

import NavItem from '../components/NavItem.jsx'

export default class Header extends React.Component {
  render() {
    return <Navbar inverse collapseOnSelect>
      <Navbar.Header>
        <Navbar.Brand>
          <a href="#"><Text message='app.title'/></a>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav>
          <NavItem to='decks'><Text message='nav.listDeck'/></NavItem>
          <NavItem to='decks/new'><Text message='nav.newDeck'/></NavItem>
          <NavItem to='prices/search'><Text message='nav.searchPrice'/></NavItem>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  }
}
