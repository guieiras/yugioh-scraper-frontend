import React from 'react'
import { Col, Row, Jumbotron } from 'react-bootstrap'
import { Text } from 'react-localize'

export default class HomePage extends React.Component {
  render() {
    return <Row>
      <Col md={12}>
        <Jumbotron>
          <h1><Text message='home.title'/></h1>
          <p><Text message='home.description'/></p>
        </Jumbotron>
      </Col>
    </Row>
  }
}
