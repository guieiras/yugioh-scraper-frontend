import React from 'react'
import { Button, FormControl, InputGroup } from 'react-bootstrap'
import FontAwesome from 'react-fontawesome'
import { Text } from 'react-localize'

import APIRequester from '../../services/APIRequester'
import Loader from '../../components/Loader.jsx'

import PricesComponent from '../../components/general/PricesComponent.jsx'

export default class PricesSearchPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = { isLoading: false, prices: [] }
    this.fetchPrices = this.fetchPrices.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  render() {
    return <div>
      <h1><Text message='prices.search.title'/></h1>
      <InputGroup>
        <FormControl type='text' name='query' onChange={this.handleChange}/>
        <InputGroup.Button>
          <Button bsStyle='default' onClick={this.fetchPrices}>
            <FontAwesome name='search'/>
          </Button>
        </InputGroup.Button>
      </InputGroup>
      <div style={{marginTop: '10px'}}>
        <Loader loading={this.state.isLoading} show={true}>
          <PricesComponent prices={this.state.prices}/>
        </Loader>
      </div>
    </div>
  }

  fetchPrices() {
    if(this.state.query) {
      this.setState({ isLoading: true })
      APIRequester.get(`prices/${encodeURIComponent(this.state.query)}`).then((prices) => {
        this.setState({ prices: prices.data, isLoading: false })
      }).catch(() => {
        this.setState({ prices: [], isLoading: false })
      })
    } else {
      this.setState({ prices: [] })
    }
  }

  handleChange(event) {
    let { name, value } = event.target 
    this.setState({ [name] : value })
  }
}
