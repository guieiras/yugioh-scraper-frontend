import React from 'react'
import { Button, Col, Row } from 'react-bootstrap'
import { Link } from 'react-router'
import { Text } from 'react-localize'
import SweetAlert from 'sweetalert-react'

import Loader from '../../components/Loader.jsx'
import TableComponent from '../../components/TableComponent.jsx'
import TagGroup from '../../components/TagGroup.jsx'
import APIRequester from '../../services/APIRequester'
import Localizer from '../../services/Localizer'

export default class DeckListPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = { idToDelete: null, decks: null, isLoading: false }
  }

  componentDidMount() {
    this.fetchDecks()
  }

  render() {
    let centerTd = {verticalAlign: 'middle'}
    let decks = this.state.decks || []

    return <Loader loading={this.state.isLoading} show={!!this.state.decks}>
      <h1><Text message='decks.list.title'/></h1>
      <Row>
        <Col md={12}>
          <TableComponent headers={this.headers}>
            { decks.map((item) => <tr key={item.id}>
              <td style={centerTd}>{item.id}</td>
              <td style={centerTd}>{item.name}</td>
              <td style={centerTd}><TagGroup tags={item.tags}/></td>
              <td style={centerTd}>
                <Link to={`/decks/${item.id}`} className={'btn btn-default btn-sm'} style={{marginRight: '5px'}}>
                  <Text message='buttons.show'/>
                </Link>
                <Button bsStyle='danger' bsSize='sm' data-id={item.id} onClick={this.setDeleteState.bind(this)}>
                  <Text message='buttons.delete'/>
                </Button>
              </td>
            </tr>) }
          </TableComponent>
        </Col>
      </Row>
      <SweetAlert
        showCancelButton
        show={!!this.state.idToDelete}
        title={Localizer.t('decks.delete.title')}
        text={Localizer.t('decks.delete.text')}
        onConfirm={() => this.handleDelete()}
        onCancel={() => this.setState({idToDelete: null})}
      />
    </Loader>
  }

  get headers() {
    let keys = ['id', 'name', 'tags', 'actions'] 
    return keys.map((item) => <Text key={item} message={`decks.list.headers.${item}`}/>)
  }

  fetchDecks() {
    this.setState({ isLoading: true })
    APIRequester.get('decks').then((response) => {
      this.setState({ decks: response.data, isLoading: false })
    }).catch(() => {
      this.setState({ isLoading: false })
    })
  }

  setDeleteState(e) {
    this.setState({ idToDelete: e.target.parentElement.getAttribute('data-id') })
  }

  handleDelete() {
    APIRequester
      .delete(`decks/${this.state.idToDelete}`)
      .then(() => { this.fetchDecks() })
      .catch(() => {})
    this.setState({ idToDelete: null })
  }
}
