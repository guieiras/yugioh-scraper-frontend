import React from 'react'
import { Button, Col, ControlLabel, FormControl, FormGroup, Row } from 'react-bootstrap'
import { Text } from 'react-localize'
import { hashHistory } from 'react-router'

import APIRequester from '../../services/APIRequester'
import YdkReader from '../../services/YdkReader'

export default class DeckNewPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = { name: '', tags: [], main: [], side: [], extra: [] }

    this.handleChange = this.handleChange.bind(this)
    this.handleFiles = this.handleFiles.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  render() {
    return <div>
      <h1><Text message='decks.new.title'/></h1>
      <form onSubmit={this.handleSubmit}>
        <FormGroup controlId='formControlsFile'>
          <ControlLabel><ControlLabel><Text message='decks.new.file'/></ControlLabel></ControlLabel>
          <FormControl type='file' name='ydk' onChange={this.handleFiles}/>
        </FormGroup>
        <Row>
          <Col md={12}>
            <FormGroup controlId='formControlsName'>
              <ControlLabel><Text message='decks.new.name'/></ControlLabel>
              <FormControl type='text' name='name' onChange={this.handleChange}/>
            </FormGroup>
            <FormGroup controlId='formControlsTags'>
              <ControlLabel><Text message='decks.new.tags'/></ControlLabel>
              <FormControl type='text' name='tags' onChange={this.handleChange}/>
              <p className='text-muted'><Text message='decks.new.tagsHint'/></p>
            </FormGroup>
          </Col>
        </Row>
        <Button type='submit'><Text message='decks.new.submit'/></Button>
      </form>
    </div>
  }

  handleChange(event) {
    let { name, value } = event.target 
    if(name == 'tags') {
      value = value.split(/,\s?/).filter((item) => !!item )
    }
    this.setState({ [name] : value })

  }

  handleFiles(event) {
    new YdkReader(event.target.files[0]).process().then((deck) => {
      const { main, extra, side } = deck
      this.setState({ main, extra, side })
    })
  }

  handleSubmit(e) {
    e.preventDefault()
    APIRequester.post('decks', this.state).then(() => {
      hashHistory.push('/decks')
    }).catch(() => {
      alert('error')
    })
  }
}
