import React from 'react'
import { Alert, Button, Well } from 'react-bootstrap'
import { Text } from 'react-localize'

import APIRequester from '../../services/APIRequester'
import Loader from '../../components/Loader.jsx'

import DeckBox from '../../components/decks/BoxComponent.jsx'
import DeckPrice from '../../components/decks/PriceComponent.jsx'
import TagGroup from '../../components/TagGroup.jsx'

export default class DeckShowPage extends React.Component {
  static get propTypes() { return { params: React.PropTypes.object }}
  
  constructor(props) {
    super(props)
    this.state = {
      deck: null,
      isLoading: false,
      requestFetch: false
    }
    this.fetchPrices = this.fetchPrices.bind(this)
  }

  componentDidMount() {
    this.setState({ isLoading: true })
    APIRequester.get(`decks/${this.props.params.id}`).then((response) => {
      this.setState({ deck: response.data, isLoading: false })
    }).catch(() => {
      this.setState({ isLoading: false })
    })
  }

  render() {
    let deck = this.state.deck || {}
    let cards = deck.cards || { main: [], extra: [], side: [] }
    let { name, tags } = deck
    let { main, side, extra } = cards
    return <Loader loading={this.state.isLoading} show={!!this.state.deck}>
      <Alert bsStyle="success" className={this.state.requestFetch ? '' : 'hidden'}>
        <Text message='decks.show.fetchAlert'/>
      </Alert>
      <Button bsStyle='primary' className={'pull-right'} onClick={this.fetchPrices}>
        <Text message='decks.show.fetchButton'/>
      </Button>
      <h1>{name}</h1>
      <TagGroup tags={tags}/>
      <Well style={{margin: '20px 0', padding: '10px'}}>
        <DeckPrice cards={[...main, ...extra, ...side]} />
      </Well>
      <DeckBox cards={cards}/>
    </Loader>
  }

  fetchPrices() {
    APIRequester.post(`decks/${this.props.params.id}`).then(() => {
      this.setState({ requestFetch: true })
    })
  }
}
