import List from './ListPage.jsx'
import New from './NewPage.jsx'
import Show from './ShowPage.jsx'

export default { List, New, Show }
