import React from 'react'
import { Col, Row } from 'react-bootstrap'
import { Text } from 'react-localize'

export default class NotFoundPage extends React.Component {
  render() {
    return <Row>
      <Col md={12} className={'text-center'}>
        <h1><Text message='notfound.title'/></h1>
        <p><Text message='notfound.description'/></p> 
      </Col>
    </Row>
  }
}
