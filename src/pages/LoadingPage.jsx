import React from 'react'
import { Col, Row } from 'react-bootstrap'
import FontAwesome from 'react-fontawesome'
import { Text } from 'react-localize'
export default class LoadingPage extends React.Component {
  render() {
    return <Row>
      <Col md={12} className={'text-center'}>
        <p><FontAwesome name='spinner' size='2x' spin/></p>
        <p style={{fontSize: '20px'}}><Text message='loading.description'/></p>
      </Col>
    </Row>
  }
}
