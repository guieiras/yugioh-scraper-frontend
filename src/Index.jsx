import React from 'react'
import ReactDOM from 'react-dom'
import Localization from 'react-localize'
import Router from './Router.jsx'
import { messages } from './locales/ptBR'

import 'bootstrap/dist/css/bootstrap.css'
import 'font-awesome/css/font-awesome.min.css'
import 'sweetalert/dist/sweetalert.css'

ReactDOM.render(<Localization messages={messages}>
  <Router />
</Localization>, document.getElementById('app'))
