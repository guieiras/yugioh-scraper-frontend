export default class YdkReader {
  constructor(file) {
    this.file = file
    this.promise = new Promise((resolve, reject) => {
      this.reject = reject
      this.resolve = resolve
    })
    this.setup()
  }

  setup() {
    this.reader = new FileReader()
    this.reader.onload = function(e) {
      this.resolve(this.readDeck(e.target.result))
    }.bind(this)
  }

  process() {
    this.reader.readAsText(this.file)
    return this.promise
  }

  readDeck(content) {
    return content
      .split(/#|!/)
      .filter((item) => item.match(/main|extra|side/))
      .map((item) => item.split(/\n/))
      .reduce((memo, value) => {
        let arr = value.filter((item) => !!item)
        let key = arr.shift().replace(/[^\w]/, '')
        memo[key] = arr.map((item) => item.replace(/[^\d]/, '')).map((id) => parseInt(id))
        return memo
      }, {})
  } 
}
