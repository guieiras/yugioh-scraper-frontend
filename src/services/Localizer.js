import { messages } from '../locales/ptBR'

export default class Localizer {
  static t(key) {
    return messages[key] || key
  }
}
