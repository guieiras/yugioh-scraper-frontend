/* globals process */
import axios from 'axios'

export default class APIRequester {
  static get(path) {
    return axios.get(this._urlFor(path))
  }

  static post(path, params) {
    return axios.post(this._urlFor(path), params)
  }

  static delete(path) {
    return axios.delete(this._urlFor(path))
  }

  static _urlFor(path) {
    return process.env.API_HOST + '/' + path
  }
}
